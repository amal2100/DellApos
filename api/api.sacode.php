<?php
session_start();
// error_reporting(0);


$api_access = false;
$headers = apache_request_headers();
// print_r($_POST);
if (isset($headers['X-Xsrf-Token'])) {
    $xsrftoken = $headers['X-Xsrf-Token'];
} 
else {
    $xsrftoken = $headers['X-XSRF-TOKEN'];
}
if (($_SESSION['XSRF-TOKEN'] == $xsrftoken) && ($_COOKIE['XSRF-TOKEN'] == $xsrftoken)) {
    $api_access = true;
}

if ($api_access) {
    
    require_once 'api_dependencies/classes/Config.php';
    require_once 'api_dependencies/classes/Db.php';
    require_once 'api_dependencies/classes/Sanitizer.php';
    require_once 'api_dependencies/classes/Partners.php';
    
    $reply = array();
    
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)) {
        $received = json_decode(file_get_contents('php://input'), true);
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)) {
        $received = $_POST;
    }

    
    $sanitizer = new Sanitizer();
    
    if (isset($received)) {
        $received_c = j_input_array_escape($received, $sanitizer);
    }
    
    $api_mode = $received_c['api_mode'];
    // print_r($_POST);
    switch ($api_mode) {
        case 'GET_DATA':
            $partner_details = new Partners('get_details',$received_c['code']);
            if($partner_details->status['flag']){
                $reply['status'] = true;
                $reply['details'] = $partner_details->details;
            }else{
                $reply['status'] = false;
                $reply['title'] = "Unknown SA code";
                $reply['description'] = "Please recheck the SA code and try again. SA codes are case sensititve";

            }
            // code...
            break;

        default:

            $reply = array('status'=>false,'title'=>'Invalid mode!','description'=>'API received a bad request. Please recheck API mode and try again.');
            // code...
            break;
    }

    echo ")]}',\n" . json_encode($reply);
} 
else {
    echo "Access denied!";
}

//sanitize the multidimensional input data
function j_input_array_escape($array, $sanitizer) {
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $array[$key] = j_input_array_escape($value, $sanitizer);
        } 
        else {
            $array[$key] = $sanitizer->clean("STM", $value);
        }
    }
    return $array;
}
?>
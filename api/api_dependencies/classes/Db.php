<?php

/**
 *
 */
abstract class DB extends Config
{
    
    //The current db we are using
    private $_current_db;
    protected $_mysqli;
    protected $_result;
    
    protected function _db_init($db_code) {
        switch ($db_code) {
            case 'WARRANTY_DB':
                // code...
                $this->_current_db = $this->_databases['warranty'];
                
                //connect the database
                $this->_db_connect();
                
                break;

            default:
                // code...
                break;
        }
    }
    
    protected function _db_connect() {
        $this->_mysqli = new mysqli($this->_connect_vars['hostname'], $this->_connect_vars['username'], $this->_connect_vars['password'], $this->_current_db);
        if (!$this->_mysqli->connect_errno) {
            
            //connectoin successful
            $this->_errors['mysqli_connect'] = false;
        } 
        else {
            
            //set the error in Config class
            // echo "Connection not fine";
            // echo "here";
            $this->_errors['mysqli_connect'] = true;
            $reply = array();
            $reply['status'] = false;
            $reply['title'] = "Unexpected Error!";
            $reply['description'] = "Site has experienced an unexpected error. Please try again in a while. If the problem persists contact site administrator.";   
            echo ")]}',\n" . json_encode($reply);
            die();
            //redirect to an error page with a human readable error description
            
        }
    }
    
    protected function _db_query($query) {
        if (!$this->_errors['mysqli_connect']) {
            
            // echo 'in here';
            $result = $this->_mysqli->query($query);
            
            if (!$result) {
                // echo $this->_mysqli->error;
                $this->error_message[] = $this->_mysqli->error;
                $this->_errors['mysqli_query'] = true;
                $this->_mysqli->close();
                $reply['status'] = false;
            $reply['title'] = "Error!";
            $reply['description'] = "Site has experienced an error while processing your request. This could be temporary problem. Please try again in a while. If the problem persists contact site administrator.";   
            echo ")]}',\n" . json_encode($reply);
            die();
            } 
            else if ($result) {
                
                //Query success
                $this->_errors['mysqli_query'] = False;
                $this->_result = $result;
            } 
            else {
                
                //Unknown error
                $this->_errors['mysqli_query'] = true;
                $result->free();
                $this->_mysqli->close();
                if (!$this->_json) {
                    header("Location: //" . $this->_redirect_location);
                    exit;
                }
            }
        }
    }
}

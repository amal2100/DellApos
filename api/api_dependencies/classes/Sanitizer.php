<?php 
/**
 * Sanitizes the data
 * I have added only two mode of sanitization
 * 1. ST - only strip and trim
 * 2. STM - strip, trim and mysqli_real_escape() 
 */
class Sanitizer extends DB {
    
    function __construct() {
        $this->_db_init('WARRANTY_DB');
        $this->_db_connect();
    }
    public function clean($mode,$unclean){
        if($mode == 'ST'){
            $this->clean = $this->st_clean($unclean);
            return $this->clean;
        }else if($mode == 'STM'){
            $st_clean = $this->st_clean($unclean);
            $this->clean = $this->stm_clean($st_clean);
            return $this->clean;
        }
        
    }
    
    public function st_clean($unclean){
        $st_clean = trim(strip_tags($unclean));
        return $st_clean;
    }
    
    public function stm_clean($st_clean){
        if(!$this->_errors['mysqli_connect']){
            return $this->_mysqli->real_escape_string($st_clean);
        }

        if(!$this->_errors['mysqli_connect']){
            $this->_mysqli->close();
        }
                
    }
    
}
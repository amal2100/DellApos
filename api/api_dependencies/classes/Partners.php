<?php 

/**
* 
*/
class Partners extends DB
{
	public $details = array();
	private $_code;

	public $status = array();


	public function __construct($mode,$code){
		switch ($mode) {
			case 'get_details':
				# code...
				$this->_code = $code;
				$this->_db_init('WARRANTY_DB');
				if(!$this->_errors['mysqli_connect']){
					$this->get_details_from_partners();
				}else{
					if($this->_DEBUG){
						$this->status['flag'] = false;
						$this->status['message'] = "Database connection failed! Error - ".$this->_mysqli->connect_error;	
					}else{
						$this->status['flag'] = false;
					}
					
				}
				break;
			
			default:
				# code...
				break;
		}
	}

	private function get_details_from_partners(){
		$query = "select company_name, city, state, pin_code, phone2 from ".$this->_tables['partners']." where sa_code = '".$this->_code."'";
		
		$this->_db_query($query);
		if(!$this->_errors['mysqli_query']){
			if($this->_result->num_rows == 0){
				// $this->status['flag'] = false;
				$this->get_details_from_partners_1();
			}else if($this->_result->num_rows == 1){
				$this->status['flag'] = true;
				$row = $this->_result->fetch_assoc();
				$this->details = $row;
				$phone_array = explode('-',$this->details['phone2']);
				$this->details['std'] = $phone_array[0];
				$this->details['lphone'] = $phone_array[1];
			}
			
			
		}else{
			if(Config::DEBUG){
				$this->status['flag'] = false;
				$this->status['message'] = "Query failed. Error - ".$this->_mysqli->error;	
			}else{
				$this->status['flag'] = false;
			}
			return false;
			
		}

	}

	//if sa code details not available in the old table, search in new table
	private function get_details_from_partners_1(){
		$query = "select new_store_name as company_name, city, state, pincode as pin_code, partner_contact_number as phone2 from ".$this->_tables['partners_1']." where codes = '".$this->_code."'";
		$this->_db_query($query);
		if(!$this->_errors['mysqli_query']){
			if($this->_result->num_rows == 0){
				$this->status['flag'] = false;
			}else if($this->_result->num_rows == 1){
				$this->status['flag'] = true;
				$row = $this->_result->fetch_assoc();
				// print_r($row);
				$this->details = $row;
				// $phone_array = explode('-',$this->details['phone2']);
				// $this->details['std'] = $phone_array[0];
				// $this->details['lphone'] = $phone_array[1];
				// $this->details['std'] = 000;
				// $this->details['lphone'] = 0000000;
			}
			
			
		}else{
			if(Config::DEBUG){
				$this->status['flag'] = false;
				$this->status['message'] = "Query failed. Error - ".$this->_mysqli->error;	
			}else{
				$this->status['flag'] = false;
			}
			return false;
			
		}
	}
	
}
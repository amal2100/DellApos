<?php 

abstract class Config {
	protected $_JSON = true;
	protected $_DEBUG = false;
	protected $_connect_vars = array(
		'hostname' => '115.124.106.120',
		'username' => 'dell_user',
		'password' => 'dell_password123'
		);

	//Add other databases here
	protected $_databases = array(
		'warranty' => 'dell_warranty'
		);

	//Add all tables here
	protected $_tables = array(
		'partners' => 'dell_partners',
		'partners_1' => 'dell_partners_9_6_2015'
		);
	
	//set initial error to true
	protected $_errors = array('mysqli_connect'=> true,'mysqli_query'=> true);
	protected $error_message = array();
}
<?php session_start(); ?>

<!DOCTYPE html>
<head>
<script src="validate.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dell Warranty Extension</title>
<link type="text/css" rel="stylesheet" href="style.css">
<link rel="stylesheet" href="selectric.css">
<link rel="stylesheet" type="text/css" href="tcal.css" />
<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="all-ie-only.css" />
<![endif]-->

<script type="text/javascript" src="tcal.js"></script> 


<script type="text/javascript" language="Javascript">

	function changeFocus(){
			
			var pin = document.getElementById('pin').value;
			if(pin.length == 6)
			{
				document.getElementById('email').focus();
			}
	}
	
	function changeFocus1(){
			
			var mobile = document.getElementById('mobile').value;
			if(mobile.length == 10)
			{
				document.getElementById('std1').focus();
			}
	}
	function changeFocus2(){
			
			var mobile = document.getElementById('std1').value;
			if(mobile.length == 5)
			{
				document.getElementById('lphone').focus();
			}
	}
	function changeFocus5(){
			
			var mobile = document.getElementById('std2').value;
			if(mobile.length == 5)
			{
				document.getElementById('plphone').focus();
			}
	}
	
	function changeFocus4(){
			
			var pin = document.getElementById('ppincode').value;
			if(pin.length == 6)
			{
				document.getElementById('std2').focus();
			}
	}
	
	function fillOffers(chosen){
				//alert(chosen);
				if(chosen == "Inspiron / AIO Desktop")
				{
					document.getElementById('offers1').innerHTML = '<select  id="offers" name="offers"><option value="" disabled="disabled" selected="selected">Select Offer</option><option value="Additional 1 Yr. (2nd Year) In-Home Warranty along with Premium Phone Support worth Rs. 4500/- now at Rs. 1999/- All Inclusive"> Additional 1 Yr.  (2nd Year) In-Home Warranty along with Premium Phone Support worth Rs. 4500/- now at Rs. 1999/- All Inclusive</option><option value="Additional 2 Yrs. (2nd & 3rd Year) In-Home Warranty along with Premium Phone Support worth Rs. 7500/- now at Rs. 3999/- All Inclusive" >Additional 2 Yrs. (2nd & 3rd Year) In-Home Warranty along with Premium Phone Support worth Rs. 7500/- now at Rs. 3999/- All Inclusive</option></select>';	
				}
				if(chosen == "Inspiron Notebook")
				{
					document.getElementById('offers1').innerHTML = '<select  id="offers" name="offers"><option value="" disabled="disabled" selected="selected">Select Offer</option><option value="Additional 1 Yr. (2nd Year) In-Home Warranty & Premium Phone Support worth Rs. 7000/- now at Rs. 2999/- All Inclusive"> Additional 1 Yr. (2nd Year) In-Home Warranty & Premium Phone Support worth Rs. 7000/- now at Rs. 2999/- All Inclusive</option><option value="Additional 1 Yr. (2nd Year) In-Home Warranty along with Accidental Damage Service & Premium Phone Support worth Rs. 8000/- now at Rs. 3999/- All Inclusive" >Additional 1 Yr. (2nd Year) In-Home Warranty along with Accidental Damage Service & Premium Phone Support worth Rs. 8000/- now at Rs. 3999/- All Inclusive</option><option value="Additional 2 Yrs. (2nd & 3rd Year) In-Home Warranty & Premium Phone Support worth Rs. 11500/- now at Rs. 4999/- All Inclusive">Additional 2 Yrs. (2nd & 3rd Year) In-Home Warranty & Premium Phone Support worth Rs. 11500/- now at Rs. 4999/- All Inclusive</option><option value="Additional 2 Yrs. (2nd & 3rd Year) In-Home Warranty along with Accidental Damage Service & Premium Phone Support worth Rs. 13000/- now at Rs. 6999/- All Inclusive">Additional 2 Yrs. (2nd & 3rd Year) In-Home Warranty along with Accidental Damage Service & Premium Phone Support worth Rs. 13000/- now at Rs. 6999/- All Inclusive</option><option value="Additional 3 Yrs. (2nd / 3rd and 4th Year) In-Home Warranty & Premium Phone Support worth Rs. 18500/- now at Rs. 9999/- All Inclusive">Additional 3 Yrs. (2nd / 3rd and 4th Year) In-Home Warranty & Premium Phone Support worth Rs. 18500/- now at Rs. 9999/- All Inclusive</option></select>';	
				}
				if(chosen == "Vostro Notebook")
				{
					document.getElementById('offers1').innerHTML = '<select  id="offers" name="offers"><option value="" disabled="disabled" selected="selected">Select Offer</option><option value="Additional 1 Yr. (2nd Year) NBD Onsite Warranty along with Accidental Damage Service worth Rs. 6500/- now at Rs. 3999/- All Inclusive">Additional 1 Yr. (2nd Year) NBD Onsite Warranty along with Accidental Damage Service worth Rs. 6500/- now at Rs. 3999/- All Inclusive</option><option value="Additional 1 Yr. (2nd Year) NBD Onsite Warranty along with Accidental Damage Service & ProSupport worth Rs. 8500/- now at Rs. 4999/- All Inclusive">Additional 1 Yr. (2nd Year) NBD Onsite Warranty along with Accidental Damage Service & ProSupport worth Rs. 8500/- now at Rs. 4999/- All Inclusive</option><option value="Additional 2 Yrs. (2nd & 3rd Year) NBD Onsite Warranty along with Accidental Damage Service worth Rs. 11500/- now at Rs. 6999/- All Inclusive">Additional 2 Yrs. (2nd & 3rd Year) NBD Onsite Warranty along with Accidental Damage Service worth Rs. 11500/- now at Rs. 6999/- All Inclusive</option><option value="Additional 2 Yrs. (2nd & 3rd Year) NBD Warranty along with Accidental Damage Service & ProSupport worth Rs. 16500/- now at Rs. 7999/- All Inclusive">Additional 2 Yrs. (2nd & 3rd Year) NBD Warranty along with Accidental Damage Service & ProSupport worth Rs. 16500/- now at Rs. 7999/- All Inclusive</option></select>';	
				}
		}
				
		function handleChange1()
		{
				document.getElementById('pfrom').value = "Dell Exclusive Store";
				document.getElementById('pfrom').readOnly = true;
				document.getElementById("purchased").innerHTML = " ";
		}
		function handleChange2()
		{
				document.getElementById('pfrom').value = "Croma";
				document.getElementById('pfrom').readOnly = true;
				document.getElementById("purchased").innerHTML = " ";
		}
		function handleChange3()
		{
				document.getElementById('pfrom').value = "Reliance Digital";
				document.getElementById('pfrom').readOnly = true;
				document.getElementById("purchased").innerHTML = " ";
		}
		function handleChange4()
		{
				document.getElementById('pfrom').value = "";
				document.getElementById('pfrom').readOnly = false;
		}
		
function validatefname()
{	
	var fname=document.getElementById('fname').value;
    var pos= fname.search(/^[a-zA-Z\s]+$/);
    document.getElementById('firstname').innerHTML = "";
    if(fname=="" || fname=="null")
   {
        document.getElementById("firstname").innerHTML = "value cannot be blank";
		return false;
   }
   else if(pos!=0)
   {
	   document.getElementById("firstname").innerHTML = "Invalid format";
		return false;
   }
}

function validatelname()
{	
	var lname=document.getElementById('lname').value;
    var pos1= lname.search(/^[a-zA-Z\s]+$/);
    document.getElementById('lastname').innerHTML = "";
    if(lname=="" || lname=="null")
   {
        document.getElementById("lastname").innerHTML = "value cannot be blank";
		return false;
   }
   else if(pos1!=0 )
   {
	   document.getElementById("lastname").innerHTML = "Invalid format";
		return false;
   }
}

function validateaddress()
{	
	var address=document.getElementById('address').value;
    var pos2= address.search(/^[a-zA-Z0-9\s,'-]*$/);
    document.getElementById('address1').innerHTML = "";
    if(address=="" || address=="null")
   {
        document.getElementById("address1").innerHTML = "value cannot be blank";
		return false;
   }
   else if(pos2!=0)
   {
	   document.getElementById("address1").innerHTML = "Invalid format";
		return false;
   }
  
}
function validatecity()
{	
	var city=document.getElementById('city').value;
    var pos3= city.search(/^[a-zA-Z\s]+$/);
    document.getElementById('city1').innerHTML = "";
    if(city=="" || city=="null")
   {
        document.getElementById("city1").innerHTML = "value cannot be blank";
		return false;
   }
   if(pos3!=0)
   {
        document.getElementById("city1").innerHTML = "Invalid format";
		return false;
   }
}


function validatecity1()
{	
	var city11=document.getElementById('pcity').value;
    var pos9= city11.search(/^[a-zA-Z\s]+$/);
    document.getElementById('city2').innerHTML = "";
    if(city11=="" || city11=="null")
   {
        document.getElementById("city2").innerHTML = "value cannot be blank";
		return false;
   }
   if(pos9!=0)
   {
        document.getElementById("city2").innerHTML = "Invalid format";
		return false;
   }
}
function validateEmail()
{	
	var email=document.getElementById('email').value;
    var pos4= email.search(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/);
    document.getElementById('email1').innerHTML = "";
    if(email=="" || email=="null")
   {
        document.getElementById("email1").innerHTML = "value cannot be blank";
		return false;
   }
   if(pos4!=0)
   {
        document.getElementById("email1").innerHTML = "Invalid format";
		return false;
   }
}

function validatepin()
{
	var pin=document.getElementById('pin').value;
	var pin1=pin.search(/^[0-9]+$/);
	var pin2= pin.search(/^[0-9]{5}.\*?$/);
	document.getElementById('pin1').innerHTML = "";
	if(pin=="" || pin=="null")
	{
		document.getElementById("pin1").innerHTML = "value cannot be left blank";
		return false;
	}
	else if(pin1!=0)
	{
		document.getElementById("pin1").innerHTML = "Invalid format";
		return false;
	}
	 else if(pin2!=0)
	{
		document.getElementById("pin1").innerHTML = "pincode should be of 6 digits";
		return false;
	}
	
}

function validatepin1()
{
	var pin=document.getElementById('ppincode').value;
	var pin1=pin.search(/^[0-9]+$/);
	var pin2= pin.search(/^[0-9]{5}.\*?$/);
	document.getElementById('pin2').innerHTML = "";
	if(pin=="" || pin=="null")
	{
		document.getElementById("pin2").innerHTML = "value cannot be left blank";
		return false;
	}
	else if(pin1!=0)
	{
		document.getElementById("pin2").innerHTML = "Invalid format";
		return false;
	}
	 else if(pin2!=0)
	{
		document.getElementById("pin2").innerHTML = "pincode should be of 6 digits";
		return false;
	}
	
}
function validatestate()
{
	var state=document.getElementById('state').value;
	var state1=state.search(/^[a-z][A-Z]+$/);
	document.getElementById('state').innerHTML = "";
	if(state=="" || state==null)
    {
        document.getElementById("state1").innerHTML = "Invalid format";
        return false;
    }

}

function validatepfrom()
{
	var from=document.getElementById('pfrom').value;
	var from1=from.search(/^[a-zA-Z\s]+$/);
	document.getElementById('purchased').innerHTML = "";
	if(from=="" || from==null)
    {
        document.getElementById("purchased").innerHTML = "value cannot be left blank";
        return false;
    }
	else if(from1!=0)
	{
		document.getElementById("purchased").innerHTML = "Invalid format";
        return false;
	}
}

function validatestate1()
{
	var state=document.getElementById('pstate').value;
	var state1=state.search(/^[a-z][A-Z]+$/);
	document.getElementById('state2').innerHTML = "";
	if(state=="" || state==null)
    {
        document.getElementById("state2").innerHTML = "Invalid format";
        return false;
    }

}
function validateservice()
{
	var ser=document.getElementById('service').value;
	var serv=ser.search(/^[a-zA-Z0-9]{7}$/);
	document.getElementById('service1').innerHTML = "";
	if(ser=="" || serv==null)
    {
        document.getElementById("service1").innerHTML = "value cannot be blank";
        return false;
    }
    else if(serv!=0)
    {
		document.getElementById("service1").innerHTML = "Invalid format";
        return false;
	}

}

function validateMobile()
{
	var mobile=document.getElementById('mobile').value;
    var mob= mobile.search(/^[0-9]{10}$/);
    document.getElementById('mobile1').innerHTML = "";
    if(mobile=="" || mobile=="null")
    {
       document.getElementById("mobile1").innerHTML = "value cannot be blank";
		return false;
    } 
    else if(mob!=0)
    {
       document.getElementById("mobile1").innerHTML = "Invalid format";
		return false;
    } 
    
}   
function validatestd()
{
	var std=document.getElementById('std1').value;
    document.getElementById('code').innerHTML = "";
    if(std=="" || std=="null")
    {
       document.getElementById("code").innerHTML = "Invalid format";
		return false;
    } 
}   

function validatestd1()
{
        var std=document.getElementById('std2').value;
    document.getElementById('code2').innerHTML = "";
    if(std==" " || std=="null")
    {
       document.getElementById("code2").innerHTML = "Invalid format";
                return false;
    } 
}   

function validatephone()
{
	var phone=document.getElementById('lphone').value;
	var phone1= phone.search(/^[0-9]{6,8}$/);
    document.getElementById('phone1').innerHTML = "";
    if(phone=="" || phone=="null")
    {
       document.getElementById("phone1").innerHTML = "value cannot be blank";
		return false;
    } 
    else if(phone1 !=0)
    {
		document.getElementById("phone1").innerHTML = "Invalid format";
		return false;
	}
}
function validatephone1()
{
        var phone=document.getElementById('plphone').value;
        var phone1= phone.search(/^[0-9]{6,8}$/);
    document.getElementById('phone2').innerHTML = "";
    if(phone=="" || phone=="null")
    {
       document.getElementById("phone2").innerHTML = "value cannot be blank";
       return false;
    } 
    else if(phone1 !=0)
    {
       document.getElementById("phone2").innerHTML = "Invalid format";
       return false;
    }
}   

function validatefile() {
    var filename = document.getElementById("file").value;
     var ext = getExt(filename);
     if(filename=="" || filename==null)
    {
        alert("Please Upload Purchase Invoice");
        document.htmlform.file.focus();

        return false;
    }
   
    else if(ext == "pdf")
    {
		return true;
	}
	else
	{ 
		alert("Please upload PDF files only.");
		return false;
	}
}

function getExt(filename) {
    var dot_pos = filename.lastIndexOf(".");
    if(dot_pos == -1)
        return "";
    return filename.substr(dot_pos+1).toLowerCase();
}

   
</script>

</head>
<body>

<form name="htmlform"  enctype="multipart/form-data" method="post" action="html_form_send.php" onsubmit="return validateForm1()">
<div class="main_container">
	<div class="container">
    	<div class="layout-header"></div>
        <div class="layout-content">
        	<div class="banner"></div>
            <div class="row" >
  <!--		<div class="error">
			<div id="firstname"></div>
			<div id="lastname"></div>
			<div id="address1"></div>
			<div id="city1"></div>
			<div id="pin1"></div>
			<div id="email1"></div>
			<div id="mobile1"></div>
			<div id="code"></div>
			<div id="phone1"></div>
			<div id="city2"></div>
			<div id="pin2"></div>
			<div id="code2"></div>
			<div id="phone2"></div>
			<div id="service1"></div>

		</div>  -->
				<?php
					$status = $_REQUEST['status'];
					if($status == "1"){ ?>
				<div class="error">Thank you for your interest in the Dell Warranty Extension program. Please check your mail for further details</div>
				
				<?php } else if($status == "invalidfile") {  ?>
				<div class="error">Invalid File</div>
				<?php } else if($status == "servicetag") {  ?>
				<div class="error">This service tag is in Use</div>
				<?php } ?>
            	<div class="col">
                
                	<h5 class="disp1">Customer Basic Information</h5>
                    <table>
                      <tr>
                        <td><label>First Name<span>*</span><b id="firstname"></b></label></td>
                        <td><label>Last Name<span>*</span><b id="lastname"></b></label></td>
                      </tr>
                      <tr>
                        <td><input id="fname" name="fname" value="<? echo $_SESSION['fname'];?>"  onblur="validatefname()"></td>
                   
                        <td><input id="lname" name="lname" value="<? echo $_SESSION['lname'];?>"  onblur="validatelname()"></td>
                       
                      </tr>
                      <tr>
                        <td ><label>Address<span>*</span><b id="address1"></b></label></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="2"><textarea id="address" name="address" onblur="validateaddress()"><? echo $_SESSION['address'];?></textarea></td>
                        
                      </tr>
                      <tr>
                        <td><label>City<span>*</span><b id="city1"></b></label></td>
                        <td><label>State<span>*</span><b id="state1"></b></label></td>
                      </tr>
                      <tr>
                        <td><input id="city" name="city" value="<? echo $_SESSION['city'];?>" onblur="validatecity()"></td>
                        <td><select name="state" id="state" >
							<?php $state = $_SESSION['state']; ?>
                        <option value="" disabled="disabled" selected="selected">Select State</option>
                        <option value="Andaman and Nicobar Islands" <?php if($state == "Andaman and Nicobar Islands") { ?> selected="selected" <?php } ?>>Andaman and Nicobar Islands</option>
						<option value="Andhra Pradesh" <?php if($state == "Andhra Pradesh") { ?> selected="selected" <?php } ?>>Andhra Pradesh</option>
						<option value="Arunachal Pradesh" <?php if($state == "Arunachal Pradesh") { ?> selected="selected" <?php } ?>>Arunachal Pradesh</option>
						<option value="Assam" <?php if($state == "Assam") { ?> selected="selected" <?php } ?>>Assam</option>
						<option value="Bihar" <?php if($state == "Bihar") { ?> selected="selected" <?php } ?>>Bihar</option>
						<option value="Chhattisgarh" <?php if($state == "Chhattisgarh") { ?> selected="selected" <?php } ?>>Chhattisgarh</option>
						<option value="Dadra and Nagar Haveli" <?php if($state == "Dadra and Nagar Haveli") { ?> selected="selected" <?php } ?>>Dadra and Nagar Haveli</option>
						<option value="Daman and Diu" <?php if($state == "Daman and Diu") { ?> selected="selected" <?php } ?>>Daman and Diu</option>
						<option value="Delhi" <?php if($state == "Delhi") { ?> selected="selected" <?php } ?>>Delhi</option>
						<option value="Goa" <?php if($state == "Goa") { ?> selected="selected" <?php } ?>>Goa</option>
                        <option value="Gujarat" <?php if($state == "Gujarat") { ?> selected="selected" <?php } ?>>Gujarat</option>
                        <option value="Haryana" <?php if($state == "Haryana") { ?> selected="selected" <?php } ?>>Haryana</option>
                        <option value="Himachal Pradesh" <?php if($state == "Himachal Pradesh") { ?> selected="selected" <?php } ?>>Himachal Pradesh</option>
                        <option value="Jammu and Kashmir" <?php if($state == "Jammu and Kashmir") { ?> selected="selected" <?php } ?>>Jammu and Kashmir</option>
                        <option value="Jharkhand" <?php if($state == "Jharkhand") { ?> selected="selected" <?php } ?>>Jharkhand</option>
                        <option value="Karnataka" <?php if($state == "Karnataka") { ?> selected="selected" <?php } ?>>Karnataka</option>
                        <option value="Kerala" <?php if($state == "Kerala") { ?> selected="selected" <?php } ?>>Kerala</option>
                        <option value="Lakshadweep" <?php if($state == "Lakshadweep") { ?> selected="selected" <?php } ?>>Lakshadweep</option>
                        <option value="Madhya Pradesh" <?php if($state == "Madhya Pradesh") { ?> selected="selected" <?php } ?>>Madhya Pradesh</option>
                        <option value="Maharashtra" <?php if($state == "Maharashtra") { ?> selected="selected" <?php } ?>>Maharashtra</option>
                        <option value="Manipur" <?php if($state == "Manipur") { ?> selected="selected" <?php } ?>>Manipur</option>
                         <option value="Meghalaya" <?php if($state == "Meghalaya") { ?> selected="selected" <?php } ?>>Meghalaya</option>
                        <option value="Mizoram" <?php if($state == "Mizoram") { ?> selected="selected" <?php } ?>>Mizoram</option>
                        <option value="Nagaland" <?php if($state == "Nagaland") { ?> selected="selected" <?php } ?>>Nagaland</option>
                        <option value="Orissa" <?php if($state == "Orissa") { ?> selected="selected" <?php } ?>>Orissa</option>
                        <option value="Pondicherry" <?php if($state == "Pondicherry") { ?> selected="selected" <?php } ?>>Pondicherry</option>
                        <option value="Punjab" <?php if($state == "Punjab") { ?> selected="selected" <?php } ?>>Punjab</option>
                        <option value="Rajasthan" <?php if($state == "Rajasthan") { ?> selected="selected" <?php } ?>>Rajasthan</option>
                        <option value="Sikkim" <?php if($state == "Sikkim") { ?> selected="selected" <?php } ?>>Sikkim</option>
                        <option value="amil Nadu" <?php if($state == "Tamil Nadu") { ?> selected="selected" <?php } ?>>Tamil Nadu</option>
                        <option value="Tripura" <?php if($state == "Tripura") { ?> selected="selected" <?php } ?>>Tripura</option>
                        <option value="Uttar Pradesh" <?php if($state == "Uttar Pradesh") { ?> selected="selected" <?php } ?>>Uttar Pradesh</option>
                        <option value="Uttarakhand" <?php if($state == "Uttarakhand") { ?> selected="selected" <?php } ?>>Uttarakhand</option>
                        <option value="West Bengal" <?php if($state == "West Bengal") { ?> selected="selected" <?php } ?>>West Bengal</option>
                        </select></td>
                       
                      </tr>
                      <tr>
                        <td ><label>Pin Code<span>*</span><b id="pin1"></b></label></td>
                        <td><label>Email ID<span>*</span><b id="email1"></b></label></td>
                      </tr>
                      <tr>
                        <td><input id="pin" name="pin" value="<? echo $_SESSION['pin'];?>" onKeyUp="changeFocus()" onblur="validatepin()" maxlength="6"></td>
                        <td><input id="email" name="email" value="<? echo $_SESSION['email'];?>" onblur="validateEmail()"></td>
                       </tr>
                      
                      
                       <tr>
                        <td><label>Mobile#<span>*</span><b id="mobile1"></b></label></td>
                        <td><label>Landline Phone<span>*</span><b id="phone1"></b></label></td>
                      </tr>
                      <tr>
                        <td><input id="mobile" name="mobile" value="<? echo $_SESSION['mobile'];?>" onKeyUp="changeFocus1()" onblur="validateMobile()" maxlength="10"></td>
                        <td><input class="std" id="std1" name="std1" maxlength="5" value="<? echo $_SESSION['std1'];?>" onKeyUp="changeFocus2()" onblur="validatestd()"> - <input class="num" id="lphone" name="lphone" maxlength="8" value="<? echo $_SESSION['lphone'];?>" onblur="validatephone()"></td>
		     </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>

                </div>
                <div>
                	
                  <div class="col col-1">
                    <h5>Product's Purchase Information</h5>
                    <table>
                      <tr>
                        <td colspan="2"></td>
                        
                      </tr>
                      <tr>
                        <td colspan="2"> 
                        <div class="rad">
                        <input class="col-2" type="radio" name="seller" value="1" id="1" onChange="handleChange1();"><label>Dell Exclusive Store</label> <input type="radio" name="seller" value="1" id="1" onChange="handleChange2();"><label>Croma</label> <input type="radio" name="seller" value="1" id="1" onChange="handleChange3();"> <label>Reliance Digital</label> <input type="radio" name="seller" value="1" id="1" onChange="handleChange4();"> <label>Other Retail Outlet</label> 
                        </div></td>
              		  </tr>
                       <tr>
                        <td><label>Purchased From<span>*</span><b id="purchased"></b></label></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><input id="pfrom" name="pfrom" value="<? echo $_SESSION['outlet'];?>" onblur="validatepfrom()"></td>
                        <td>&nbsp;</td>
                      </tr>
                       <tr>
                        <td><label>City<span>*</span><b id="city2"></b></label></td>
                        <td><label>State<span>*</span><b id="state2"></b></label></td>
                      </tr>
                      <tr>
                        <td><input  id="pcity" name="pcity" value="<? echo $_SESSION['city'];?>" onblur="validatecity1()"></td>
                        <td><select id="pstate" name="pstate">
							<?php $state = $_SESSION['pstate']; ?>
                        <option value="" disabled="disabled" selected="selected">Select State</option>
                        <option value="Andaman and Nicobar Islands" <?php if($state == "Andaman and Nicobar Islands") { ?> selected="selected" <?php } ?>>Andaman and Nicobar Islands</option>
						<option value="Andhra Pradesh" <?php if($state == "Andhra Pradesh") { ?> selected="selected" <?php } ?>>Andhra Pradesh</option>
						<option value="Arunachal Pradesh" <?php if($state == "Arunachal Pradesh") { ?> selected="selected" <?php } ?>>Arunachal Pradesh</option>
						<option value="Assam" <?php if($state == "Assam") { ?> selected="selected" <?php } ?>>Assam</option>
						<option value="Bihar" <?php if($state == "Bihar") { ?> selected="selected" <?php } ?>>Bihar</option>
						<option value="Chhattisgarh" <?php if($state == "Chhattisgarh") { ?> selected="selected" <?php } ?>>Chhattisgarh</option>
						<option value="Dadra and Nagar Haveli" <?php if($state == "Dadra and Nagar Haveli") { ?> selected="selected" <?php } ?>>Dadra and Nagar Haveli</option>
						<option value="Daman and Diu" <?php if($state == "Daman and Diu") { ?> selected="selected" <?php } ?>>Daman and Diu</option>
						<option value="Delhi" <?php if($state == "Delhi") { ?> selected="selected" <?php } ?>>Delhi</option>
						<option value="Goa" <?php if($state == "Goa") { ?> selected="selected" <?php } ?>>Goa</option>
                        <option value="Gujarat" <?php if($state == "Gujarat") { ?> selected="selected" <?php } ?>>Gujarat</option>
                        <option value="Haryana" <?php if($state == "Haryana") { ?> selected="selected" <?php } ?>>Haryana</option>
                        <option value="Himachal Pradesh" <?php if($state == "Himachal Pradesh") { ?> selected="selected" <?php } ?>>Himachal Pradesh</option>
                        <option value="Jammu and Kashmir" <?php if($state == "Jammu and Kashmir") { ?> selected="selected" <?php } ?>>Jammu and Kashmir</option>
                        <option value="Jharkhand" <?php if($state == "Jharkhand") { ?> selected="selected" <?php } ?>>Jharkhand</option>
                        <option value="Karnataka" <?php if($state == "Karnataka") { ?> selected="selected" <?php } ?>>Karnataka</option>
                        <option value="Kerala" <?php if($state == "Kerala") { ?> selected="selected" <?php } ?>>Kerala</option>
                        <option value="Lakshadweep" <?php if($state == "Lakshadweep") { ?> selected="selected" <?php } ?>>Lakshadweep</option>
                        <option value="Madhya Pradesh" <?php if($state == "Madhya Pradesh") { ?> selected="selected" <?php } ?>>Madhya Pradesh</option>
                        <option value="Maharashtra" <?php if($state == "Maharashtra") { ?> selected="selected" <?php } ?>>Maharashtra</option>
                        <option value="Manipur" <?php if($state == "Manipur") { ?> selected="selected" <?php } ?>>Manipur</option>
                         <option value="Meghalaya" <?php if($state == "Meghalaya") { ?> selected="selected" <?php } ?>>Meghalaya</option>
                        <option value="Mizoram" <?php if($state == "Mizoram") { ?> selected="selected" <?php } ?>>Mizoram</option>
                        <option value="Nagaland" <?php if($state == "Nagaland") { ?> selected="selected" <?php } ?>>Nagaland</option>
                        <option value="Orissa" <?php if($state == "Orissa") { ?> selected="selected" <?php } ?>>Orissa</option>
                        <option value="Pondicherry" <?php if($state == "Pondicherry") { ?> selected="selected" <?php } ?>>Pondicherry</option>
                        <option value="Punjab" <?php if($state == "Punjab") { ?> selected="selected" <?php } ?>>Punjab</option>
                        <option value="Rajasthan" <?php if($state == "Rajasthan") { ?> selected="selected" <?php } ?>>Rajasthan</option>
                        <option value="Sikkim" <?php if($state == "Sikkim") { ?> selected="selected" <?php } ?>>Sikkim</option>
                        <option value="amil Nadu" <?php if($state == "Tamil Nadu") { ?> selected="selected" <?php } ?>>Tamil Nadu</option>
                        <option value="Tripura" <?php if($state == "Tripura") { ?> selected="selected" <?php } ?>>Tripura</option>
                        <option value="Uttar Pradesh" <?php if($state == "Uttar Pradesh") { ?> selected="selected" <?php } ?>>Uttar Pradesh</option>
                        <option value="Uttarakhand" <?php if($state == "Uttarakhand") { ?> selected="selected" <?php } ?>>Uttarakhand</option>
                        <option value="West Bengal" <?php if($state == "West Bengal") { ?> selected="selected" <?php } ?>>West Bengal</option>
                        </select>
                        </td>
                       
                      </tr>
                      <tr>
                        <td><label>Pincode<span>*</span><b id="pin2"></b></label></td>
                        <td><label>Landline Phone<span>*</span><b id="phone2"></b></label></td>
                      </tr>
                       <tr>
                        <td><input id="ppincode" name="ppincode" maxlength="6" value="<? echo $_SESSION['ppincode'];?>" onKeyUp="changeFocus4()" onblur="validatepin1()"></td>
                        <td><input class="std" id="std2" name="std2" maxlength="5" value="<? echo $_SESSION['std2'];?>" onKeyUp="changeFocus5()" onblur="validatestd1()"> - <input class="num" id="plphone" name="plphone" maxlength="8" value="<? echo $_SESSION['plphone'];?>" onblur="validatephone1()"></td>
	
                      </tr>
                       <tr>
                         <td><label>Date of Purchase<span>*</span></label></td>
                         <td><label>Service Tag<span>*</span><b id="service1"></b></label></td>
                       </tr>
                       <tr>
                         <td><input id="date" name="date" class="tcal" value="<?php echo $_SESSION['sdate']; ?>" /></td>
                         <td><input id="service" maxlength="7" name="service" value="<? echo $_SESSION['service'];?>" onblur="validateservice()"></td>
                        
                       </tr>
                       <tr>
                         <td><label>Upload the Purchase Invoice here</label> </td>
                         <td><input type="file" class="upload" id="file" name="file" ></td>
                       </tr>
                     
                    </table>

                    </div>
                </div>
                <div class="col col-2">
                    <h5 class="disp">Warranty Pack Selection</h5>
                    <table border="0">
                       <tr>
                        <td><label>Product (Model)<span>*</span></label></td>
                        <td><select id="product" name="product" onChange="fillOffers(document.htmlform.product.options[document.htmlform.product.selectedIndex].value);">
							<?php $product = $_SESSION['product']; ?>
                        	<option value="" disabled="disabled" selected="selected">Select Model</option>
                        	<option value="Inspiron / AIO Desktop" <?php if($product == "Inspiron / AIO Desktop") { ?> selected="selected"<?php } ?>>Inspiron / AIO Desktop</option>
                            <option value="Inspiron Notebook"  <?php if($product == "Inspiron Notebook") { ?> selected="selected"<?php } ?>>Inspiron Notebook</option>
                            <option value="Vostro Notebook"  <?php if($product == "Vostro Notebook") { ?> selected="selected"<?php } ?>>Vostro Notebook</option>
                            
                            
                        </select></td>
                        </tr>
                     <tr>
                        <td>
                        <label>Offers (Extension Pack)<span>*</span></label>
                        
                        </td>
                        <td id="offers1">
                        <select class="my_dropdown" id="offers" name="offers">
							<option value="" disabled="disabled" selected="selected">Select Offer</option>
                            </select>
                            </td>
                      </tr>
                    </table>

                    
                    </div>
            </div>
             <div class="ab">
             	<input type="checkbox" name="info" id="info"> <p>Please accept to confirm the information is validated by you</p>
             </div>
            <div class="but">
            <input type="submit" value="Submit to Request" name="submit" class="submit"></div>
        </div>
        <div class="footer">
                    	<div class="foot">
                    	<div class="foot_left">© 2014 Regenersis (India) Pvt Ltd (Formerly  Digicomp Complete Solutions Ltd.)- Dell Authorized Service Center</div>
                        <div class="foot_right">
                        <a href="about.html">About Regenersis</a>
                        <a href="contact.html">Contact Us</a>
                        <a href="Terms.html">Dell Warranty Terms & Condition</a><br>

			<a href="privacy.html" > Privacy Policy </a>
		<a href="login.php" class="las" > Partner Login </a>                        

 </div><div class="powered"> 
 	<div class="title">We Accept</div>
    <a href="http://www.mastercard.com/in/consumer/index.html" target="_blank"><img src="img/master.jpg"></a> <a href="http://www.visa-asia.com/ap/gateway/flash/index.html" target="_blank"><img src="img/visa.jpg"></a>
 </div>
 <div class="powered"> 
 	<div class="title">Powered by</div>
    <img src="img/bill.jpg"></a> 
 </div>
  <div class="foot_left1">This site works best on IE version 9.0 and above and Chrome browser</div>
            	
 </div>
               
            
            </div>
        <div class="layout-footer"></div>
    </div>
</div>


 </body>
</html>


<?php unset($_SESSION); ?>

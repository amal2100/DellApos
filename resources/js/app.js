'use strict';

//AngularJS app level module. Do not write other JS codes in this file
angular.module('dellApp',['dellApp.controllers','ngMaterial','ngMessages','dellApp.factories']);
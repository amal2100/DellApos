'use strict';

angular.module('dellApp.factories', [])
    .factory('sacodeFactory', ['$http', function($http) {
        var factory = {};
        var url = 'api/api.sacode.php';
        factory.pullDetails = function(code) {
                    return $http({
                        url: url,
                        method: 'POST',
                        headers : {
                            'Content-type' : 'application/json'
                        },
                        data: {
                            'api_mode': 'GET_DATA',
                            'code': code
                        },
                        cache: false
                    });
                }

          return factory;      
    }]);
'use strict';
angular.module('dellApp.controllers', []).controller('warrantyCtrl', ['$scope', '$window', 'sacodeFactory', '$mdDialog', function($scope, $window, sacodeFactory, $mdDialog) {
    $scope.widgets = {
        displayDateSection: true,
        displayFormSection: true,
        formStyle0_45: false,
        formStyle45_365: false,
        reset: function() {
            this.displayDateSection = true;
            this.displayFormSection = true;
            this.formStyle0_45 = false;
            this.formStyle45_365 = false;
        }
    };
    $scope.dialog = {
        showAlert: function(title, message) {
            $mdDialog.show($mdDialog.alert().parent(angular.element(document.querySelector('body'))).clickOutsideToClose(true).title(title).content(message).ariaLabel('Info').ok('Close').targetEvent())
        }
    };
    $scope.purchase = {
        date: "",
        verify: function() {
            $scope.sacode['code'] = "";
            $scope.customer.store['name'] = "";
            $scope.customer.store['city'] = "";
            $scope.customer.store['state'] = "";
            $scope.customer.store['pincode'] = "";
            $scope.customer.store['serviceTag'] = "";
            $scope.purchase.date = jQuery('#datetimepicker1 input').val();
            $scope.customer.product.productSelected = "";
            var todaysDate = new Date();
            var purchaseDate = new Date($scope.purchase.date);
            var secondsDifference = (todaysDate.getTime() - purchaseDate.getTime());
            var daysDifference = Math.ceil(((secondsDifference) / (24 * 3600 * 1000)) - 1);
            console.log(daysDifference);
            if ((daysDifference < 46) && (daysDifference >= 0)) {
                $scope.widgets.displayFormSection = true;
                $scope.widgets.displayDateSection = true;
                $scope.widgets.formStyle0_45 = true;
                $scope.widgets.formStyle45_365 = false;
            } else if ((daysDifference >= 46) && (daysDifference <= 365)) {
                $scope.widgets.displayFormSection = true;
                $scope.widgets.displayDateSection = true;
                $scope.widgets.formStyle0_45 = false;
                $scope.widgets.formStyle45_365 = true;
            } else {
                var title = "Invalid date!";
                var message = "The date you have entered is not valid for warranty pack subscription."
                $scope.dialog.showAlert(title, message);
                $scope.purchase.date = "";
            }
        }
    };
    $scope.customer = {
        data: {
            firstName: "",
            lastName: "",
            address: "",
            city: "",
            state: "",
            pincode: "",
            email: "",
            confirmEmail: "",
            mobile: "",
            stdCode: "",
            landPhone: ""
        },
        states: ['Andaman and Nicobar Islands', 'Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chhattisgarh', 'Dadra and Nagar Haveli', 'Daman and Diu', 'Delhi', 'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jammu and Kashmir', 'Jharkhand', 'Karnataka', 'Kerala', 'Lakshadweep', 'Madhya Pradesh', 'Maharashtra', 'Manipur', 'Meghalaya', 'Mizoram', 'Nagaland', 'Orissa', 'Pondicherry', 'Punjab', 'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Tripura', 'Uttar Pradesh', 'Uttarakhand', 'West Bengal'],
        store: {
            //Adding store properties here for future reference
            name: "",
            city: "",
            state: "",
            pincode: "",
            serviceTag: ""
        },
        product: {
            productSelected: "",
            offerSelected: ""
        },
        termsOfService: false,
        validateFile: function() {
            var filename = document.getElementById("file").value;
            var ext = filename.split('.').pop();
            // var ext = getExt(filename);
            if (filename == "" || filename == null) {
                
                document.htmlform.file.value = "";
                document.htmlform.file.focus();
                var t_no_file = "Select a file";
                var m_no_file = "Please upload invoice copy";
                $scope.dialog.showAlert(t_no_file, m_no_file);
                return false;
            } else if ((ext == "pdf") || (ext == "png") || (ext == "jpeg") || (ext == "jpg")) {
                return true;
            } else {
                document.htmlform.file.value = "";
                var t_invalid_file = "Invalid file";
                var m_invalid_file = "Please upload image or pdf files only";
                $scope.dialog.showAlert(t_invalid_file, m_invalid_file);
                return false;
            }
        },
        purchaseAction: function($event) {
            $event.preventDefault();
            var allCustomerData = false;
            var allStoreData = false;
            var pattern1 = new RegExp('/^[" "]+$/');
            var pattern2 = new RegExp('/^[" "]+$/');
            for (var key in $scope.customer.data) {
                if ($scope.customer.data.hasOwnProperty(key)) {
                    if ((typeof $scope.customer.data[key] == 'undefined') || ($scope.customer.data[key] == "")) {
                        allCustomerData = false;
                        var t_customer = "Incomplete data";
                        var m_customer = "A required information is missing."
                        $scope.dialog.showAlert(t_customer, m_customer);
                        return;
                    } else {
                        allCustomerData = true;
                    }
                }
            }
            for (var data in $scope.customer.store) {
                if ($scope.customer.store.hasOwnProperty(data)) {
                    if ((typeof $scope.customer.store[data] == 'undefined') || ($scope.customer.store[data] == "")) {
                        allStoreData = false;
                        var t_customer = "Incomplete data";
                        var m_customer = "A required information is missing."
                        $scope.dialog.showAlert(t_customer, m_customer);
                        return;
                    } else {
                        allStoreData = true;
                    }
                }
                console.log($scope.customer.store[data]);
            }
            if (allCustomerData && allStoreData) {
                if ($scope.customer.data.email == $scope.customer.data.confirmEmail) {
                    if ($scope.customer.termsOfService) {
                        if ($scope.customer.store['name'] == "") {
                            var t_pfrom = "Missing field";
                            var m_from = "A required field is missing";
                            $scope.dialog.showAlert(t_pfrom, m_from);
                        } else {
                            if ($scope.customer.validateFile()) {
                                document.getElementsByName('htmlform')[0].submit();
                            }
                        }
                    } else {
                        var t_tos = "Agree to Terms and Conditions";
                        var m_tos = "Please check the terms and conditions box before submitting the form";
                        $scope.dialog.showAlert(t_tos, m_tos);
                    }
                } else {
                    var title = "Emails not same";
                    var message = "Please enter the same email address in both email and confirm email field."
                    $scope.dialog.showAlert(title, message);
                }
            } else {
                var title = "Missing required field";
                var message = "A required filed is missing."
                $scope.dialog.showAlert(title, message);
            }
        },
        onRedirect: function() {
            $scope.customer.data = $window.customer.data;
            $scope.customer.store = $window.customer.store;
        }
    };
    if (window.hasOwnProperty('customer')) {
        $scope.customer.onRedirect();
    }
    //reset on wrong date selection
    angular.copy($scope.customer.store, $scope.productForm);
    /**
     * [offerType description]
     * @type {Object}
     */
    $scope.offerType = {
        apos: {
            offer1: {
                name: "APOS offer1",
                visible: true
            },
            offer2: {
                name: "APOS offer2",
                visible: true
            },
            offer3: {
                name: "APOS offer3",
                visible: true
            }
        },
        normal: {
            offer0: {
                name: "No offer available",
                visible: true
            },
            offer1: { //Inspiron Desktop / AIO Desktop
                name: "Additional 1 Yr. (2nd Year) In-Home Warranty along with 24x7 Phone Support worth Rs. 4500/- now at Rs. 1999/- All Inclusive",
                visible: true
            },
            offer2: { //Inspiron Desktop / AIO Desktop
                name: "Additional 2 Yrs. (2nd & 3rd Year) In-Home Warranty along with 24x7 Phone Support worth Rs. 7500/- now at Rs. 3999/- All Inclusive",
                visible: true
            },
            offer3: { //Inspiron Notebook
                name: "Additional 1 Yr. (2nd Year) In-Home Warranty along with Accidental Damage Service and 24x7 Phone Support worth Rs. 8000/- now at Rs. 3999/- All Inclusive",
                visible: true
            },
            offer4: { //Inspiron Notebook
                name: "Additional 2 Yrs. (2nd & 3rd Year) In-Home Warranty along with 24x7 Phone Support worth Rs. 11500/- now at Rs. 4999/- All Inclusive",
                visible: true
            },
            offer5: { //Inspiron Notebook
                name: "Additional 2 Yrs. (2nd & 3rd Year) In-Home Warranty along with Accidental Damage Service and 24x7 Phone Support worth Rs. 13000/- now at Rs. 6999/- All Inclusive",
                visible: true
            },
            offer6: { //Inspiron Notebook
                name: "1 Yr. (1st Year Standalone) Accidental Damage Protection only worth Rs. 2999/- now at Rs. 999/- All Inclusive",
                visible: true
            },
            offer7: { //Inspiron Notebook
                name: "2 Yr. (1st & 2nd Year Standalone) Accidental Damage Protection only worth Rs. 4999/- now at Rs. 1999/- All Inclusive",
                visible: true
            },
            offer8: { //Vostro Notebook
                name: "Additional 1 Yr. (2nd Year) NBD Onsite Warranty along with Accidental Damage Service worth Rs. 6500/- now at Rs. 3999/- All Inclusive",
                visible: true
            },
            offer9: { //Vostro Notebook
                name: "Additional 1 Yr. (2nd Year) NBD Onsite Warranty along with Accidental Damage Service & ProSupport worth Rs. 8500/- now at Rs. 4999/- All Inclusive",
                visible: true
            },
            offer10: { //Vostro Notebook
                name: "Additional 2 Yrs. (2nd & 3rd Year) NBD Onsite Warranty along with Accidental Damage Service worth Rs. 11500/- now at Rs. 6999/- All Inclusive",
                visible: true
            },
            offer11: { //Vostro Notebook
                name: "Additional 2 Yrs. (2nd & 3rd Year) NBD Warranty along with Accidental Damage Service & ProSupport worth Rs. 16500/- now at Rs. 7999/- All Inclusive",
                visible: true
            },
            offer12: { //OptiPlex Desktop
                name: "Additional 1 Yr. (2nd Year) NBD Onsite Warranty worth Rs. 3000/- now at Rs. 1999/- All Inclusive",
                visible: true
            },
            offer13: { //OptiPlex Desktop
                name: "Additional 2 Yrs. (2nd & 3rd Year) NBD Onsite Warranty worth Rs. 5000/- now at Rs. 3999/- All Inclusive",
                visible: true
            },
            offer14: { //Fixed Workstation
                name: "Additional 1 Yr. (2nd Year) NBD Onsite Warranty worth Rs. 7500/- now at Rs. 3499/- All Inclusive",
                visible: true
            },
            offer15: { //Fixed Workstation
                name: "Additional 2 Yrs. (2nd & 3rd Year) NBD Onsite Warranty worth Rs. 10000/- now at Rs.6999/- All Inclusive",
                visible: true
            },
            offer16: { //Latitude notebook
                name: "Additional 1 Yr. (2nd Year) NBD Onsite Warranty worth Rs. 10000/- now at Rs. 3999/- All Inclusive",
                visible: true
            },
            offer17: { //Latitude notebook
                name: "Additional 2 Yrs. (2nd & 3rd Year) NBD Onsite Warranty worth Rs. 13000/- now at Rs. 7999/- All Inclusive",
                visible: true
            },
            offer18: { //Mobile Workstation
                name: "Additional 1 Yr. (2nd Year) NBD Onsite Warranty worth Rs. 8000/- now at Rs. 4999/- All Inclusive",
                visible: true
            },
            offer19: { //Mobile workstation
                name: "Additional 2 Yrs. (2nd & 3rd Year) NBD Onsite Warranty worth Rs. 11000/- now at Rs. 9999/- All Inclusive",
                visible: true
            }
        }
    };
    /**
     * **********WARNING**********
     * OfferType object need to be defined before the products
     * [products description]
     * @type {Object}
     */
    $scope.products = {
        normal: {
            model1: {
                name: "Inspiron Desktop / AIO Desktop",
                offers: {
                    apos: {
                        offer1: $scope.offerType.apos.offer1
                    },
                    normal: {
                        offer1: $scope.offerType.normal.offer1,
                        offer2: $scope.offerType.normal.offer2
                    }
                }
            },
            model2: {
                name: "Inspiron Notebook",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer3,
                        offer2: $scope.offerType.normal.offer4,
                        offer3: $scope.offerType.normal.offer5,
                        offer4: $scope.offerType.normal.offer6,
                        offer5: $scope.offerType.normal.offer7
                    }
                }
            },
            model3: {
                id: 3,
                name: "Vostro Notebook",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer8,
                        offer2: $scope.offerType.normal.offer9,
                        offer3: $scope.offerType.normal.offer10,
                        offer4: $scope.offerType.normal.offer11,
                    }
                }
            },
            model4: {
                id: 4,
                name: "OptiPlex Desktop",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer12,
                        offer2: $scope.offerType.normal.offer13
                    }
                }
            },
            model5: {
                name: "Fixed Workstation",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer14,
                        offer2: $scope.offerType.normal.offer15
                    }
                }
            },
            model6: {
                name: "Latitude Notebook",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer16,
                        offer2: $scope.offerType.normal.offer17
                    }
                }
            },
            model7: {
                name: "Mobile Workstation",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer18,
                        offer2: $scope.offerType.normal.offer19
                    }
                }
            }
        },
        apos: {
            model1: {
                name: "APOS - Inspiron Desktop / AIO Desktop",
                offers: {
                    apos: {
                        offer1: $scope.offerType.apos.offer1
                    },
                    normal: {
                        offer1: $scope.offerType.normal.offer1,
                        offer2: $scope.offerType.normal.offer2
                    }
                }
            },
            model2: {
                name: "APOS - Inspiron Notebook",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer3,
                        offer2: $scope.offerType.normal.offer4,
                        offer3: $scope.offerType.normal.offer5,
                        offer4: $scope.offerType.normal.offer6,
                        offer5: $scope.offerType.normal.offer7
                    }
                }
            },
            model3: {
                name: "APOS - Vostro Notebook",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer8,
                        offer2: $scope.offerType.normal.offer9,
                        offer3: $scope.offerType.normal.offer10,
                        offer4: $scope.offerType.normal.offer11,
                    }
                }
            },
            model4: {
                name: "APOS - OptiPlex Desktop",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer12,
                        offer2: $scope.offerType.normal.offer13
                    }
                }
            },
            model5: {
                name: "APOS - Fixed Workstation",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer14,
                        offer2: $scope.offerType.normal.offer15
                    }
                }
            },
            model6: {
                name: "APOS - Latitude Notebook",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer16,
                        offer2: $scope.offerType.normal.offer17
                    }
                }
            },
            model7: {
                name: "APOS - Mobile Workstation",
                offers: {
                    apos: {},
                    normal: {
                        offer1: $scope.offerType.normal.offer18,
                        offer2: $scope.offerType.normal.offer19
                    }
                }
            }
        }
    };
    /**
     * Api --
     */
    $scope.sacode = {
            code: "",
            spinner: false,
            getData: function(code) {
                if ((code == "") || (code == " ") || (code == "undefined")) {
                    var title = "SA Code required";
                    var message = "Please enter an SA code and try again.";
                    $scope.dialog.showAlert(title, message);
                } else {
                    $scope.sacode.spinner = true;
                    sacodeFactory.pullDetails(code).success(function(data) {
                        if (!data.status) {
                            $scope.sacode.spinner = false;
                            $scope.dialog.showAlert(data.title, data.description);
                        }
                        if (data.status) {
                            $scope.sacode.spinner = false;
                            $scope.customer.store.name = data.details.company_name;
                            $scope.customer.store.city = data.details.city;
                            $scope.customer.store.state = data.details.state;
                            $scope.customer.store.pincode = data.details.pin_code;
                        }
                    }).error(function(data) {
                        $scope.sacode.spinner = false;
                        var t_error = "Connection problem!";
                        var t_message = "Sorry, we are unable to connect at this moment. Please try again later";
                        $scope.dialog.showAlert(t_error, t_message);
                    });
                }
            }
        }
        //setting up value attribute coming from php
        // $scope.$apply();
}]);
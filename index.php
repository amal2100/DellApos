<?php
session_start();

require_once('dbconfig.php');
if(!isset($_SESSION['XSRF-TOKEN'])){
$_SESSION['XSRF-TOKEN'] = sha1(time());
setcookie('XSRF-TOKEN',$_SESSION['XSRF-TOKEN'],0,'/');
}
if(isset($_REQUEST['id'])){
    // echo "In here";
    $id = $_REQUEST['id'];
    $id = mysql_real_escape_string($id);
    $orderSql = "SELECT * FROM warranty_orders where id = ".$id;
    $orderRes = mysql_query($orderSql);
    $dataOrder = mysql_fetch_assoc($orderRes);
                                
    $user_email = $dataOrder['email'];
                                        $amount = $dataOrder['amount'];
                                        $customerid = $dataOrder['orderno'];
                                        $product = $dataOrder['product'];
                                        $offer = $dataOrder['offer'];
                                        $name = $dataOrder['firstname']." ".$dataOrder['lastname'];
                                        $mobile = $dataOrder['mobile'];
                                        $fname = $dataOrder['firstname'];
                                        $lname = $dataOrder['lastname'];
                                        $address = $dataOrder['address'];
                                        $city = $dataOrder['city'];
                                        $state = $dataOrder['state'];
                                        $pin = $dataOrder['pincode'];
                                        $landline = $dataOrder['landline'];

                                        $landline_array = explode('-',$landline);

                                        //New code
                                        $std1 = $landline_array[0];
                                        $lphone = $landline_array[1];

                                        $outlet = $dataOrder['outlet'];
                                        $date = $dataOrder['seller_date'];
                                        $service_tag = $dataOrder['service_tag'];
                                        $seller_state = $dataOrder['seller_state'];

                                        //new code
                                        $seller_city = $dataOrder['seller_city'];
                                        $seller_pin = $dataOrder['seller_pincode'];
                                        $landline2 = $dataOrder['seller_landline'];
                                        $land2Array = explode("-",$landline2);

                                        //new code
                                        $std2 = $land2Array[0];
                                        $plphone = $land2Array[1];
                                // }
                                // print_r($dataOrder);
}//if isset($_request['id'])
                    

                   
?>
<!Doctype html>
<html lang="en" ng-app="dellApp" style="background-color:#E9E9E9">
    <head>
        <title>Dell Warranty extension</title>
        <meta name="viewport" content="initial-scale=1">
        <!-- styles -->
        <link rel="stylesheet" href="resources/css/index_dell_apos_2.css">
        <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="bower_components/moment/min/moment.min.js"></script>
        <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
        <!-- Angular JS Material design cdn -->
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/angular_material/0.10.1/angular-material.min.css">
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic">
    </head>
    <body style="background-color:#E9E9E9">

    <?php if(isset($_REQUEST['id'])) : ?>
        <script>
            //Transfering the form data values on redirect to angularJS
            //This is a work around to support the legacy code.
            //The values of these properties are echoed by php.
            window.customer = {
                data : {
                    firstName : "<?php if(isset($fname)) echo $fname;  ?>",
                    lastName  : "<?php if(isset($lname)) echo $lname; ?>",
                    address : "<?php if(isset($address)) echo $address; ?>",
                    city: "<?php if(isset($city)) echo $city; ?>",
                    state: "<?php if(isset($state)) echo $state; ?>",
                    pincode: "<?php if(isset($pin)) echo $pin; ?>",
                    email: "<?php if(isset($user_email)) echo $user_email; ?>",
                    confirmEmail: "",
                    mobile: "<?php if(isset($mobile)) echo $mobile; ?>",
                    stdCode: "<?php if(isset($std1)) echo $std1; ?>",
                    landPhone: "<?php if(isset($lphone)) echo $lphone; ?>"
                },
                store : {
                    name: "<?php if(isset($outlet)) echo $outlet;  ?>",
                    city: "<?php if(isset($seller_city)) echo $seller_city;  ?>",
                    state: "<?php if(isset($seller_state)) echo $seller_state;  ?>",
                    pincode: "<?php if(isset($seller_pin)) echo $seller_pin;  ?>",
                    stdCode: "<?php if(isset($std2)) echo $std2;  ?>",
                    landPhone: "<?php if(isset($plphone)) echo $plphone;  ?>",
                    serviceTag: "<?php if(isset($service_tag)) echo $service_tag;  ?>"
                },
                product: {
                    productSelected: "<?php if(isset($product)) echo $product; ?>",
                    offerSelected: "<?php if(isset($offer)) echo $offer; ?>"
                }    
                
            }

        </script>
    <?php endif; ?>
        <md-content layout="column">
        <header>
            <!--Note: Image loaded via css. Keep the header block empty -->
        </header>
        <!-- warrantyCtrl executes in this div block. Using ng-cloak to display only on complete load -->
        <div>
            <?php
            if(isset($_REQUEST['status'])){
                $status = $_REQUEST['status'];
                    if($status == "1"){ ?>
                <div class="error">Thank you for your interest in the Dell Warranty Extension program. Please check your mail for further details</div>
                
                <?php } else if($status == "invalidfile") {  ?>
                <div class="error">Invalid File</div>
                <?php } else if($status == "servicetag") {  ?>
                <div class="error">This service tag is in Use</div>
                <?php }  
            }          
            ?>
        </div>
        <div id="content" ng-controller="warrantyCtrl" ng-cloak>
            <!-- <h1>Dell After Point Of Sale Warranty Extension</h1>
            -->
            <section ng-show="widgets.displayFormSection">
                <form name="htmlform" enctype="multipart/form-data"
                    method="post" action="html_form_send.php"
                    layout="row" layout-wrap ng-submit="customer.purchaseAction($event)">
                    <div class="formBlocks" flex="48">
                        <h3 md-padding class="disp1">Customer basic information</h3>
                        <div>
                            <!-- Customer chunk start Dynamically include customer form -->
                            <div layout="row" layout-wrap>

                                <div flex="50" class="sapcing-bottom">
                                    <label>First Name<span class="red-color">*</span></label>
                                    <input type="text" ng-trim="false" ng-pattern="/^[a-zA-Z][a-zA-Z\s]+$/" name="fname" ng-model="customer.data.firstName" required>
                                    <div class="formMessages" ng-messages="htmlform.fname.$error" ng-show="htmlform.fname.$dirty" role="alert">
                                        <div ng-message="required">Your first name is required!</div>
                                        <div ng-message="pattern">First name should have at least 2 characters.</div>
                                    </div>
                                </div>
                                <div flex="50" class="sapcing-bottom">
                                    <label>Last Name<span class="red-color">*</span></label><br>
                                    <input type="text" ng-trim="false" ng-pattern="/^[a-zA-Z\s]+$/"  name="lname" ng-model="customer.data.lastName" required>
                                    <div class="formMessages" ng-messages="htmlform.lname.$error" ng-show="htmlform.lname.$dirty" role="alert">
                                        <div ng-message="required">Your last name is required!</div>
                                        <div ng-message="pattern">Only letters are allowed.</div>
                                    </div>
                                </div>
                                <div flex="50" class="sapcing-bottom">
                                    <label>Address<span class="red-color">*</span></label><br>
                                    <textarea ng-trim="false" name="address" ng-pattern="/^[a-zA-Z0-9\s,\/#()\.'-]*$/" ng-model="customer.data.address" required ng-maxlength="150"></textarea>
                                    <div class="formMessages" ng-messages="htmlform.address.$error" ng-show="htmlform.address.$dirty" role="alert">
                                        <div ng-message="required">Your address is required!</div>
                                        <div ng-message="md-maxlength">That's too long!</div>
                                        <div ng-message="pattern">Invalid format!</div>
                                    </div>
                                </div>
                                <div flex="45" class="sapcing-bottom">
                                    <label>City<span class="red-color">*</span></label><br>
                                    <input type="text" ng-trim="false" name="city" ng-pattern="/^[a-zA-Z][a-zA-Z\s]+$/" ng-model="customer.data.city" required>
                                    <div class="formMessages" ng-messages="htmlform.city.$error" ng-show="htmlform.city.$dirty" role="alert">
                                        <div ng-message="required">This is required!</div>
                                        <div ng-message="pattern">Invalid format!</div>
                                    </div>
                                </div>
                                <div flex="50" class="sapcing-bottom">
                                    <label>Select a state<span class="red-color">*</span></label><br>
                                    <select name="state" ng-model="customer.data.state" required>
                                        <option value="" disabled selected>Select State</option>
                                        <option ng-value="opt" ng-repeat="opt in customer.states">{{ opt }}</option>
                                    </select>
                                </div>
                                <div flex="45" class="sapcing-bottom">
                                    <label>Pincode<span class="red-color">*</span></label><br>
                                    <input type="text" ng-trim="false" name="pin" ng-model="customer.data.pincode" ng-pattern="/^[0-9]{6}$/" required maxlength="6">
                                    <div class="formMessages" ng-messages="htmlform.pin.$error" ng-show="htmlform.pin.$dirty" role="alert">
                                        <div ng-message="pattern">Pincode has to be a 6 digit number.</div>
                                        <div ng-message="required">Pincode is required.</div>
                                    </div>
                                </div>
                                <div flex="50" class="sapcing-bottom">
                                    <label>Email address<span class="red-color">*</span></label><br>
                                    <input ng-pattern="/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/" type="email" name="email" ng-model="customer.data.email" required>
                                    <div class="formMessages" ng-messages="htmlform.email.$error" ng-show="htmlform.email.$dirty" role="alert">
                                        <div ng-message="required">Your email address is required.</div>
                                        <div ng-message="pattern">Invalid email!</div>
                                    </div>
                                </div>
                                <div flex="45" class="sapcing-bottom">
                                    <label>Confirm your email<span class="red-color">*</span></label><br>
                                    <input type="email" ng-pattern="/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/" name="confirm_email" ng-model="customer.data.confirmEmail" required>
                                    <div class="formMessages" ng-messages="htmlform.confirm_email.$error" ng-show="htmlform.confirm_email.$dirty" role="alert">
                                        <div ng-message="pattern">Invalid format.</div>
                                    </div>
                                </div>
                                <div flex="50" class="sapcing-bottom">
                                    <label>Mobile number<span class="red-color">*</span></label><br>
                                    <input type="text" ng-trim="false" name="mobile" ng-model="customer.data.mobile" ng-pattern="/^[1-9][0-9]{9}$/" required maxlength="10">
                                    <div class="formMessages" ng-messages="htmlform.mobile.$error" ng-show="htmlform.mobile.$dirty" role="alert">
                                        <div ng-message="pattern">Mobile number has to be a 10 digit number.</div>
                                    </div>
                                </div>
                                <div flex="50" class="space-ifan ">
                                    <label>Land phone<span class="red-color">*</span></label><br>
                                    <div layout="row" layout-wrap style="width:200px">
                                        <input style="width:29%" type="text" name="std1" maxlength="5" ng-trim="false" ng-model="customer.data.stdCode" ng-pattern="/^[0-9]{1,5}$/" required><span>-</span>
                                        <input style="width:65%" type="text" name="lphone" maxlength="9" ng-trim="false" ng-model="customer.data.landPhone" ng-pattern="/^[0-9]{6,9}$/" required>
                                    </div>
                                    <div class="formMessages" ng-messages="htmlform.std1.$error" ng-show="htmlform.std1.$dirty" role="alert">
                                        <div ng-message="pattern">Std code has to be a 3 to 5 digit number.</div>
                                    </div>
                                    <div class="formMessages" ng-messages="htmlform.lphone.$error" ng-show="htmlform.lphone.$dirty" role="alert">
                                        <div ng-message="pattern">Landline number has to be a 6 to 9 digit number.</div>
                                    </div>
                                </div>
                                </div><!--Customer chunk end -->
                            </div>
                        </div>
                        <div class="formBlocks" flex="48">
                            <h3 class="disp1">Products purchase information</h3>
                            
                            
                            <div><!--Product form chunk begin
                                Dynamically include product chunk of form -->
                                <div layout="row" layout-wrap>
                                    
                                    <div layout="row" layout-wrap ng-disabled>
                                        
                                        <section ng-show="widgets.displayDateSection">
                                <div class="form-group ">
                                    <label flex="100">Select your purchase date</label><br>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input ng-change="purchase.verify()" style="height:30px" id="datatime" name="date" type='text' class="form-control" ng-model="purchase.date" required/>
                                        <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
            
                                    </div>
                                    <div class="controls" ng-show="widgets.formStyle0_45">
                                        <input class="col-2" type="radio" name="seller" id="dell_exclusive_store" value="Dell Exclusive Store" ng-model="customer.store.name" ng-required="widgets.formStyle0_45">
                                        <label for="dell_exclusive_store">Dell Exclusive Store</label>
                                        <input class="col-2" type="radio" name="seller" id="croma" value="Croma" ng-model="customer.store.name" ng-required="widgets.formStyle0_45">
                                        <label for="croma">Croma</label>
                                        <br>
                                        <input class="col-2" type="radio" name="seller" id="reliance_digital" value="Reliance digital" ng-model="customer.store.name" ng-required="widgets.formStyle0_45">
                                        <label for="reliance_digital">Reliance digital</label>
                                        <input ng-click="customer.store.name=''" class="col-2" type="radio" name="seller" id="other_retail_outlets" value="Other retail outlets" ng-required="widgets.formStyle0_45">
                                        <label for="other_retail_outlets">Other retail outlets</label>
                                    </div>
                                    <div class="controls1 " ng-show="widgets.formStyle45_365">
                                            <label class="sa-code">SA Code<span class="red-color">*</span><i style="position:absolute; top:25px; left:180px;" ng-show="sacode.spinner" class="fa fa-spinner fa-spin"></i></label>
                                            <input type="text" ng-trim="false" maxlength="8" ng-model="sacode.code" ng-blur="sacode.getData(sacode.code)" ng-required="widgets.formStyle45_365">
                                    </div>
                                </div>
                            </section>
                                        
                                        <div flex="100"  class="space-ifan ng-scope purchased">
                                            <label>Purchased from<span class="red-color">*</span></label>
                                            <input type="text" name="pfrom" ng-trim="true" ng-model="customer.store.name" ng-readonly="widgets.formStyle45_365 || ((widgets.formStyle0_45) && ((customer.store.name == 'Dell Exclusive Store') || (customer.store.name == 'Croma') || (customer.store.name == 'Reliance digital'))) || (!purchase.date)" required>
                                            <div class="formMessages" ng-messages="htmlform.pfrom.$error" ng-show="htmlform.pfrom.$dirty">
                                                <div ng-message="required">This is required.</div>
                                            </div>
                                        </div>
                                        <div flex="50" class="space-ifan ng-scope">
                                            <label>City<span class="red-color">*</span></label>
                                            <input type="text" name="pcity" ng-trim="false" ng-model="customer.store.city" ng-readonly="widgets.formStyle45_365 || (!purchase.date)" required>
                                            <div class="formMessages" ng-messages="htmlform.pcity.$error" ng-show="htmlform.pcity.$dirty">
                                                <div ng-message="required">Store city is required.</div>
                                            </div>
                                        </div>
                                        <div ng-if="!widgets.formStyle45_365" flex="50" class="space-ifan ng-scope">
                                            <label>State<span class="red-color">*</span></label>
                                            <select name="pstate" ng-model="customer.store.state" ng-readonly="(!purchase.date)" required>
                                                <option ng-repeat="state in customer.states" ng-value="state">{{ state }}</md-option>
                                                </select>
                                            </div>
                                            <div ng-if="widgets.formStyle45_365" flex="50" class="space-ifan ng-scope">
                                            <label>State<span class="red-color">*</span></label>
                                            <input type="text" name="pstate" ng-trim="false" ng-readonly="widgets.formStyle45_365" ng-model="customer.store.state" required>
                                            <div class="formMessages" ng-messages="htmlform.pstate.$error" ng-show="htmlform.pstate.$dirty">
                                                <div ng-message="required">Store state is required.</div>
                                            </div>
                                        </div>
                                            <div flex="50" class="space-ifan ng-scope">
                                                <label>Pincode<span class="red-color">*</span></label>
                                                <input type="text" name="ppincode" maxlength="6" ng-trim="false" ng-model="customer.store.pincode" ng-pattern="/^[0-9]{6}$/" ng-readonly="widgets.formStyle45_365 || (!purchase.date)" required>
                                                <div class="formMessages" ng-messages="htmlform.ppincode.$error" ng-show="htmlform.ppincode.$dirty" role="alert">
                                                    <div ng-message="pattern">Pincode has to be a 6 digit number.</div>
                                                </div>
                                            </div>
                                            <div flex="50" class="space-ifan ng-scope land-ph">
                                                <div layout="row" layout-wrap>
                                                    <label flex="100">Land Phone</label>
                                                    <input style="width:29%" type="text" maxlength="5" name="std2" ng-pattern="/^[0-9]{1,5}$/" ng-readonly="widgets.formStyle45_365 || (!purchase.date)"> <span> - </span>
                                                    <input style="width:58%" type="text" maxlength="10" name="plphone" ng-pattern="/^[0-9]{6,9}$/" ng-readonly="widgets.formStyle45_365 || (!purchase.date)">
                                                </div>
                                                <div class="formMessages" ng-messages="htmlform.std2.$error" ng-show="htmlform.std2.$dirty" role="alert">
                                                    <div ng-message="pattern">Std code has to be a 3 to 5 digit number.</div>
                                                </div>
                                                <div class="formMessages" ng-messages="htmlform.plphone.$error" ng-show="htmlform.plphone.$dirty" role="alert">
                                                    <div ng-message="pattern">Landline number has to be a 6 to 9 digit number.</div>
                                                </div>
                                            </div>
                                            <div flex="50" class="space-ifan ng-scope">
                                                <label>Service Tag<span class="red-color">*</span></label><br>
                                                <input type="text" name="service" ng-disabled="(!purchase.date)" ng-trim="false" maxlength="7" ng-minlength="7" ng-maxlength="7" ng-pattern="/^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i" ng-model="customer.store.serviceTag" required>
                                                <div class="formMessages" ng-messages="htmlform.service.$error" ng-show="htmlform.service.$dirty" role="alert">
                                                    <div ng-message="pattern">Service tag has to be 7 characters and alpha numeric.</div>
                                                    <div ng-message="minlength">Service tag has to be 7 characters and alpha numeric.</div>
                                                    <div ng-message="maxlength">Service tag has to be 7 characters and alpha numeric.</div>
                                                </div>
                                            </div>
                                            <div flex="100" id="file-upload">
                                                <div layout="row" layout-wrap>
                                                    <label flex="50">Upload the purchase invoice here</label>
                                                    <input flex="45" type="file" id="file" ng-disabled="(!purchase.date)" name="file" id="invoiceFile" accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div><!--Product form chunk end -->
                                </div>
                                <div class="formBlocks" flex="100">
                                    <h3 class="disp1" >Warranty pack selection</h3>
                                    <div>
                                        <!--Warranty chunk begin Dynamically include warranty chunk of form -->
                                        <div class="product-model" layout="row" layout-wrap>

                                            <!-- Dud logic -->
                                            <div flex="100" style="position:relative" ng-if="((!widgets.formStyle0_45) && (!widgets.formStyle45_365))">
                                                <table>
                                                    <tr>
                                                        <th  style="padding-bottom:5px; padding-top:5px;"><label>Product model<span class="red-color">*</span></label></th>
                                                        <td>
                                                            <select id="product" name="product" required disabled>
                                                                <option value="" disabled selected>Select model</option>

                                                            </select></td>
                                                        </tr>
                                                        <tr>
                                                            <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                            <td id="offers1"><select disabled>
                                                                <option value="" disabled selected>Select Offer</option>
                                                            </select></td>
                                                        </tr>
                                                    </table>
                                                    <div ng-click="dialog.showAlert('Select a date!','Please select a purchase date to see the warranty extension packs.')" style="position: absolute; top: 0px; left:0px; width:100%; height:100%;">

                                                    </div>
                                                </div>
                                                <!-- Logic Normal offers  -->
                                                <div flex="100" ng-if="widgets.formStyle0_45">
                                                    <table>
                                                        <tr>
                                                            <th  style="padding-bottom:5px; padding-top:5px;"><label>Product model<span class="red-color">*</span></label></th>
                                                            <td>
                                                                <select id="product" name="product" ng-model="customer.product.productSelected" required>
                                                                    <option value="" disabled selected>Select model</option>
                                                                    <option ng-value="value.name" ng-repeat = "(key, value) in products.normal">{{ value.name }}</option>
                                                                </select></td>
                                                            </tr>
                                                            <tr ng-hide="customer.product.productSelected">
                                                                <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                <td id="offers1"><select>
                                                                    <option value="" disabled selected>Select Offer</option>
                                                                </select></td>
                                                            </tr>
                                                            <tr ng-if="(customer.product.productSelected == 'Inspiron Desktop / AIO Desktop')">
                                                                <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                    <option value="" disabled selected>Select Offer</option>
                                                                    <option ng-show="value.visible" ng-value="value.name" ng-repeat="(key, value) in products.normal.model1.offers.normal">{{ value.name }}</option>
                                                                </select></td>
                                                            </tr>
                                                            <tr ng-if="(customer.product.productSelected == 'Inspiron Notebook')">
                                                                <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                    <option value="" disabled selected>Select Offer</option>
                                                                    <option ng-show="value.visible" ng-value="value.name" ng-repeat="(key, value) in products.normal.model2.offers.normal">{{ value.name }}</option>
                                                                </select></td>
                                                            </tr>
                                                            <tr ng-if="(customer.product.productSelected == 'Vostro Notebook')">
                                                                <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                    <option value="" disabled selected>Select Offer</option>
                                                                    <option ng-show="value.visible" ng-value="value.name" ng-repeat="(key, value) in products.normal.model3.offers.normal">{{ value.name }}</option>
                                                                </select></td>
                                                            </tr>
                                                            <tr ng-if="(customer.product.productSelected == 'OptiPlex Desktop')">
                                                                <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                    <option value="" disabled selected>Select Offer</option>
                                                                    <option ng-show="value.visible" ng-value="value.name" ng-repeat="(key, value) in products.normal.model4.offers.normal">{{ value.name }}</option>
                                                                </select></td>
                                                            </tr>
                                                            <tr ng-if="(customer.product.productSelected == 'Fixed Workstation')">
                                                                <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                    <option value="" disabled selected>Select Offer</option>
                                                                    <option ng-show="value.visible" ng-value="value.name" ng-repeat="(key, value) in products.normal.model5.offers.normal">{{ value.name }}</option>
                                                                </select></td>
                                                            </tr>
                                                            <tr ng-if="(customer.product.productSelected == 'Latitude Notebook')">
                                                                <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                    <option value="" disabled selected>Select Offer</option>
                                                                    <option ng-show="value.visible" ng-value="value.name" ng-repeat="(key, value) in products.normal.model6.offers.normal">{{ value.name }}</option>
                                                                </select></td>
                                                            </tr>
                                                            <tr ng-if="(customer.product.productSelected == 'Mobile Workstation')">
                                                                <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                    <option value="" disabled selected>Select Offer</option>
                                                                    <option ng-show="value.visible" ng-value="value.name" ng-repeat="(key, value) in products.normal.model7.offers.normal">{{ value.name }}</option>
                                                                </select></td>
                                                            </tr>
                                                        </table>
                                                        </div><!--- Normal offers -->


                                                        <!-- Logic Apos offers  -->
                                                        <div flex="100" ng-if="widgets.formStyle45_365">
                                                            <table>
                                                                <tr>
                                                                    <th  style="padding-bottom:5px; padding-top:5px;"><label>Product model<span class="red-color">*</span></label></th>
                                                                    <td>
                                                                        <select id="product" name="product" ng-model="customer.product.productSelected" required>
                                                                            <option value="" disabled selected>Select model</option>
                                                                            <option ng-value="value.name" ng-repeat = "(key, value) in products.apos">{{ value.name }}</option>
                                                                        </select></td>
                                                                    </tr>
                                                                    <tr ng-hide="customer.product.productSelected">
                                                                        <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                        <td id="offers1"><select>
                                                                            <option value="" disabled selected>Select Offer</option>
                                                                        </select></td>
                                                                    </tr>
                                                                    <tr ng-if="(customer.product.productSelected == 'APOS - Inspiron Desktop / AIO Desktop')">
                                                                        <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                        <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                            <option value="" disabled selected>Select Offer</option>
                                                                            <option ng-value="value.name" ng-show="value.visible" ng-repeat="(key, value) in products.apos.model1.offers.apos">{{ value.name }}</option>
                                                                        </select></td>
                                                                    </tr>
                                                                    <tr ng-if="(customer.product.productSelected == 'APOS - Inspiron Notebook')">
                                                                        <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                        <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                            <option value="" disabled selected>Select Offer</option>
                                                                            <option ng-value="value.name" ng-show="value.visible" ng-repeat="(key, value) in products.apos.model2.offers.apos">{{ value.name }}</option>
                                                                        </select></td>
                                                                    </tr>
                                                                    <tr ng-if="(customer.product.productSelected == 'APOS - Vostro Notebook')">
                                                                        <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                        <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                            <option value="" disabled selected>Select Offer</option>
                                                                            <option ng-value="value.name" ng-show="value.visible" ng-repeat="(key, value) in products.apos.model3.offers.apos">{{ value.name }}</option>
                                                                        </select></td>
                                                                    </tr>
                                                                    <tr ng-if="(customer.product.productSelected == 'APOS - OptiPlex Desktop')">
                                                                        <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                        <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                            <option value="" disabled selected>Select Offer</option>
                                                                            <option ng-value="value.name" ng-show="value.visible" ng-repeat="(key, value) in products.apos.model4.offers.apos">{{ value.name }}</option>
                                                                        </select></td>
                                                                    </tr>
                                                                    <tr ng-if="(customer.product.productSelected == 'APOS - Fixed Workstation')">
                                                                        <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                        <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                            <option value="" disabled selected>Select Offer</option>
                                                                            <option ng-value="value.name" ng-show="value.visible" ng-repeat="(key, value) in products.apos.model5.offers.apos">{{ value.name }}</option>
                                                                        </select></td>
                                                                    </tr>
                                                                    <tr ng-if="(customer.product.productSelected == 'APOS - Latitude Notebook')">
                                                                        <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                        <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                            <option value="" disabled selected>Select Offer</option>
                                                                            <option ng-value="value.name" ng-show="value.visible" ng-repeat="(key, value) in products.apos.model6.offers.apos">{{ value.name }}</option>
                                                                        </select></td>
                                                                    </tr>
                                                                    <tr ng-if="(customer.product.productSelected == 'APOS - Mobile Workstation')">
                                                                        <th><label>Offers (Extension pack)<span class="red-color">*</span></label></th>
                                                                        <td id="offers1"><select ng-model="customer.product.offerSelected" name="offers" required>
                                                                            <option value="" disabled selected>Select Offer</option>
                                                                            <option ng-value="value.name" ng-show="value.visible" ng-repeat="(key, value) in products.apos.model7.offers.apos">{{ value.name }}</option>
                                                                        </select></td>
                                                                    </tr>
                                                                </table>
                                                                </div><!--- Apos offers -->

                                                            </div>
                                                            <!-- warranty chunk end-->
                                                        </div>
                                                    </div>
                                                    <div flex="100" class="accept">
                                                        <div>
                                                            <md-checkbox ng-model="customer.termsOfService" required>
                                                            I accept and agree to the Dell warranty terms and conditions. Further, I agree that the submitted
                                                            information is correct.
                                                            </md-checkbox>
                                                        </div>
                                                    </div>
                                                    <div flex="100" class="request-btn">
                                                        <button class="blue-button">
                                                        Submit to request
                                                        </button>
                                                    </div>
                                                </form>
                                            </section>
                                            </div><!--content-->
                                            <div class="footer">
                                                <div class="foot">
                                                    <div class="foot_left">&copy; 2015 Regenersis (India) Pvt Ltd (Formerly  Digicomp Complete Solutions Ltd.)- Dell Authorized Service Center</div>
                                                    <div class="foot_right">
                                                        <a href="about.html">About Regenersis</a>
                                                        <a href="contact.html">Contact Us</a>
                                                        <a href="Terms.html">Dell Warranty Terms &amp; Condition</a><br>
                                                        <a href="privacy.html"> Privacy Policy </a>
                                                        <a href="login.php" class="las"> Partner Login </a>
                                                    </div><div class="powered">
                                                    <div class="title">We Accept</div>
                                                    <a href="http://www.mastercard.com/in/consumer/index.html" target="_blank" class="visa"><img src="resources/img/master.jpg" alt="Master card"></a>
                                                    <a href="http://www.visa-asia.com/ap/gateway/flash/index.html" target="_blank"  class="mastercard">  <img src="resources/img/visa.jpg"></a>
                                                </div>
                                                <div class="powered">
                                                    <div class="title1">Powered by</div>
                                                    <img src="resources/img/bill.jpg" class="powered-img">
                                                </div>
                                                <div class="foot_left1">This site works best on IE version 9.0 and above and Chrome browser</div>
                                            </div>
                                        </div>
                                        </md-content>
                                        <!-- Angular Material Dependencies -->
                                        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
                                        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-animate.min.js"></script>
                                        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-aria.min.js"></script>
                                        <script src="//ajax.googleapis.com/ajax/libs/angular_material/0.10.1/angular-material.min.js"></script>
                                        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-messages.js"></script>
                                        <!-- Custom AngularJS scripts -->
                                        <script src="resources/js/app.js"></script>
                                        <script src="resources/js/controllers.js"></script>
                                        <script src="resources/js/factories.js"></script>
                                        <!-- Customer js scripts -->
                                        <script type="text/javascript">
                                                                        //Do not remove this code... necessary for angular to get the
                                                                        //change event
                                                                            $(function () {
                                                                                        $('#datetimepicker1').datetimepicker({
                                                                                            keepOpen : false,
                                                                                            format : 'MMM DD YYYY',
                                                                                                useCurrent : false
                                                                                        });
                                                                                        $(document).on('dp.change',function(){
                                                                                            $('#datetimepicker1 > input').change();
                                                                                            });
                                                                                    });
                                        </script>
                                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
                                    </body>
                                </html>